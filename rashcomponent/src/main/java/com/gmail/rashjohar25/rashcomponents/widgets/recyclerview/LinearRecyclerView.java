package com.gmail.rashjohar25.rashcomponents.widgets.recyclerview;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;

import com.gmail.rashjohar25.rashcomponents.extended.AbstractRecyclerView;


public class LinearRecyclerView extends AbstractRecyclerView {
    public LinearRecyclerView(Context context) {
        super(context,false);
        sameLinear(context,null);
    }

    public LinearRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs,false);
        sameLinear(context,attrs);
    }

    public LinearRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle,false);
        sameLinear(context,attrs);
    }
    private void sameLinear(Context context, @Nullable AttributeSet attrs)
    {
        setLayoutManager(new LinearLayoutManager(context,getOrientedby(),getReverseLayout()));
    }
}