package com.gmail.rashjohar25.rashcomponents.extended;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.gmail.rashjohar25.rashcomponents.R;

/**
 * Created by Johars on 2/28/2018.
 */

public abstract class AbstractRecyclerView extends RecyclerView {
    private Boolean reverseLayout=false;
    private int orientedby= 1;
    private Boolean isGridView=false;
    public AbstractRecyclerView(Context context,Boolean isGridView) {
        super(context);
        sameProperties(context,null);
    }

    public AbstractRecyclerView(Context context,@Nullable AttributeSet attrs,Boolean isGridView) {
        super(context, attrs);
        sameProperties(context,attrs);
    }

    public AbstractRecyclerView(Context context,@Nullable AttributeSet attrs, int defStyle,Boolean isGridView) {
        super(context,attrs, defStyle);
        sameProperties(context,attrs);
    }

    public int getOrientedby() {
        return orientedby;
    }

    public void setOrientedby(int orientedby) {
        this.orientedby = orientedby;
        invalidate();
        requestLayout();
    }
    public Boolean getReverseLayout() {
        return reverseLayout;
    }

    public void setReverseLayout(Boolean reverseLayout) {
        this.reverseLayout = reverseLayout;
        invalidate();
        requestLayout();
    }
    private void sameProperties(Context context, @Nullable AttributeSet attrs)
    {
        TypedArray a;
        if(isGridView) {
            if(attrs!=null) {
                a = context.getTheme().obtainStyledAttributes(
                        attrs,
                        R.styleable.GridRecyclerView,
                        0, 0);
                try {
                    reverseLayout = a.getBoolean(R.styleable.GridRecyclerView_reverselayout, false);
                    orientedby = a.getInteger(R.styleable.GridRecyclerView_orientedby, GridLayoutManager.VERTICAL);
                } finally {
                    a.recycle();
                }
            }
        } else {
            if(attrs!=null) {
                a = context.getTheme().obtainStyledAttributes(
                        attrs,
                        R.styleable.LinearRecyclerView,
                        0, 0);
                try {
                    reverseLayout = a.getBoolean(R.styleable.LinearRecyclerView_reverselayout, false);
                    orientedby = a.getInteger(R.styleable.LinearRecyclerView_orientedby, LinearLayoutManager.VERTICAL);
                } finally {
                    a.recycle();
                }
            }
        }

    }
}
