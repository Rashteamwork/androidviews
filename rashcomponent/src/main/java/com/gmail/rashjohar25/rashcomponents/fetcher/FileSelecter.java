package com.gmail.rashjohar25.rashcomponents.fetcher;

import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.provider.MediaStore;

import com.gmail.rashjohar25.rashcomponents.fetcher.datatypes.MusicItem;
import com.gmail.rashjohar25.rashcomponents.fetcher.datatypes.VideoItem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileSelecter {
    private Context context;
    public static final double SIZE_FACTOR = 1024.0;
    public static final String CAMERA_IMAGE_BUCKET_NAME = Environment.getExternalStorageDirectory().toString() + "/DCIM/Camera";
    public FileSelecter(Context context)
    {
        this.context=context;
    }
    public List<File> getFilesList(File file)
    {
        List<File> fileList = new ArrayList<File>();
        File[] files = file.listFiles();
        fileList.clear();
        for (File f : files) {
            if (f.isDirectory()) {
                fileList.addAll(getFilesList(f));
            } else {
                fileList.add(f);
            }
        }
        return fileList;
    }
    public String getBucketId() {
        return String.valueOf(CAMERA_IMAGE_BUCKET_NAME.toLowerCase().hashCode());
    }
    public String ConvertSizeInStandard(long sizeinBytes)
    {
        if(sizeinBytes>=SIZE_FACTOR) {
            if(sizeinBytes>=SIZE_FACTOR*SIZE_FACTOR) {
                if(sizeinBytes>=SIZE_FACTOR*SIZE_FACTOR*SIZE_FACTOR) {
                    return (sizeinBytes/(SIZE_FACTOR*SIZE_FACTOR*SIZE_FACTOR))+"GB";
                }else {
                    return (sizeinBytes/(SIZE_FACTOR*SIZE_FACTOR))+" MB";
                }
            }else {
                return (sizeinBytes/SIZE_FACTOR)+"KB";
            }
        }else{
            return sizeinBytes+" B";
        }
    }
    public List<VideoItem> getVideoList()
    {
        String[] projection = {
                MediaStore.Video.Media._ID,
                MediaStore.Video.Media.ARTIST,
                MediaStore.Video.Media.TITLE,
                MediaStore.Video.Media.DATA,
                MediaStore.Video.Media.DISPLAY_NAME,
                MediaStore.Video.Media.DURATION,
                MediaStore.Video.Media.SIZE
        };
        final Cursor cursor = context.getContentResolver().query(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                null);
        List<VideoItem> songs = new ArrayList<VideoItem>();
        while(cursor.moveToNext()) {
            songs.add(new VideoItem(cursor.getString(0),cursor.getString(3),
                    cursor.getString(2),cursor.getString(1),cursor.getString(6),
                    cursor.getString(4),cursor.getString(5)));
        }
        cursor.close();
        return songs;
    }
    public List<MusicItem> getMusicList() throws NullPointerException
    {
        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";

        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.SIZE,
        };
        final Cursor cursor = context.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                null,
                null);

        List<MusicItem> songs = new ArrayList<MusicItem>();

        while(cursor.moveToNext()) {
            songs.add(new MusicItem(cursor.getString(0),cursor.getString(3),
                    cursor.getString(2),cursor.getString(1),cursor.getString(6),
                    cursor.getString(4),cursor.getString(5)));
        }
        cursor.close();
        return songs;
    }
    public List<String> getGalleryImages() {
        final String[] projection = { MediaStore.Images.Media.DATA };
        final Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                null);
        ArrayList<String> result = new ArrayList<String>(cursor.getCount());
        if (cursor.moveToFirst()) {
            final int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            do {
                final String data = cursor.getString(dataColumn);
                result.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

    public List<String> getCameraImages() throws NullPointerException{
        final String[] projection = { MediaStore.Images.Media.DATA };
        final String selection = MediaStore.Images.Media.BUCKET_ID + " = ?";
        final String[] selectionArgs = { getBucketId() };
        final Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                selectionArgs,
                null);
        ArrayList<String> result = new ArrayList<String>(cursor.getCount());
        if (cursor.moveToFirst()) {
            final int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            do {
                final String data = cursor.getString(dataColumn);
                result.add(data);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }

}
