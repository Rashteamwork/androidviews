package com.gmail.rashjohar25.rashcomponents.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

abstract class ListAppCompatActivity extends AppCompatActivity {
    public ListAppCompatActivity() {
        throw new RuntimeException("Stub!");
    }

    protected void onListItemClick(ListView l, View v, int position, long id) {
        throw new RuntimeException("Stub!");
    }

    protected void onRestoreInstanceState(Bundle state) {
        throw new RuntimeException("Stub!");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        throw new RuntimeException("Stub!");
    }

    public void onContentChanged() {
        throw new RuntimeException("Stub!");
    }

    public void setListAdapter(ListAdapter adapter) {
        throw new RuntimeException("Stub!");
    }

    public void setSelection(int position) {
        throw new RuntimeException("Stub!");
    }

    public int getSelectedItemPosition() {
        throw new RuntimeException("Stub!");
    }

    public long getSelectedItemId() {
        throw new RuntimeException("Stub!");
    }

    public ListView getListView() {
        throw new RuntimeException("Stub!");
    }

    public ListAdapter getListAdapter() {
        throw new RuntimeException("Stub!");
    }
}
