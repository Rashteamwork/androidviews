package com.gmail.rashjohar25.rashcomponents.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ListView;

import java.util.List;

/**
 * Created by Johars on 4/6/2017.
 */

public class RashActivity extends ListAppCompatActivity implements PreferenceFragment.OnPreferenceStartFragmentCallback {
    public static final String EXTRA_NO_HEADERS = ":android:no_headers";
    public static final String EXTRA_SHOW_FRAGMENT = ":android:show_fragment";
    public static final String EXTRA_SHOW_FRAGMENT_ARGUMENTS = ":android:show_fragment_args";
    public static final String EXTRA_SHOW_FRAGMENT_SHORT_TITLE = ":android:show_fragment_short_title";
    public static final String EXTRA_SHOW_FRAGMENT_TITLE = ":android:show_fragment_title";
    public static final long HEADER_ID_UNDEFINED = -1L;

    public RashActivity() {
        throw new RuntimeException("Stub!");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        throw new RuntimeException("Stub!");
    }

    public boolean hasHeaders() {
        throw new RuntimeException("Stub!");
    }

    public boolean isMultiPane() {
        throw new RuntimeException("Stub!");
    }

    public boolean onIsMultiPane() {
        throw new RuntimeException("Stub!");
    }

    public boolean onIsHidingHeaders() {
        throw new RuntimeException("Stub!");
    }

    public PreferenceActivity.Header onGetInitialHeader() {
        throw new RuntimeException("Stub!");
    }

    public PreferenceActivity.Header onGetNewHeader() {
        throw new RuntimeException("Stub!");
    }

    public void onBuildHeaders(List<PreferenceActivity.Header> target) {
        throw new RuntimeException("Stub!");
    }

    public void invalidateHeaders() {
        throw new RuntimeException("Stub!");
    }

    public void loadHeadersFromResource(int resid, List<PreferenceActivity.Header> target) {
        throw new RuntimeException("Stub!");
    }

    protected boolean isValidFragment(String fragmentName) {
        throw new RuntimeException("Stub!");
    }

    public void setListFooter(View view) {
        throw new RuntimeException("Stub!");
    }
    @Override
    protected void onStop() {
        super.onStop();
        throw new RuntimeException("Stub!");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        throw new RuntimeException("Stub!");
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        throw new RuntimeException("Stub!");
    }

    protected void onRestoreInstanceState(Bundle state) {
        throw new RuntimeException("Stub!");
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        throw new RuntimeException("Stub!");
    }

    public void onContentChanged() {
        throw new RuntimeException("Stub!");
    }

    protected void onListItemClick(ListView l, View v, int position, long id) {
        throw new RuntimeException("Stub!");
    }

    public void onHeaderClick(PreferenceActivity.Header header, int position) {
        throw new RuntimeException("Stub!");
    }

    public Intent onBuildStartFragmentIntent(String fragmentName, Bundle args, int titleRes, int shortTitleRes) {
        throw new RuntimeException("Stub!");
    }

    public void startWithFragment(String fragmentName, Bundle args, Fragment resultTo, int resultRequestCode) {
        throw new RuntimeException("Stub!");
    }

    public void startWithFragment(String fragmentName, Bundle args, Fragment resultTo, int resultRequestCode, int titleRes, int shortTitleRes) {
        throw new RuntimeException("Stub!");
    }

    public void showBreadCrumbs(CharSequence title, CharSequence shortTitle) {
        throw new RuntimeException("Stub!");
    }

    public void setParentTitle(CharSequence title, CharSequence shortTitle, View.OnClickListener listener) {
        throw new RuntimeException("Stub!");
    }

    public void switchToHeader(String fragmentName, Bundle args) {
        throw new RuntimeException("Stub!");
    }

    public void switchToHeader(PreferenceActivity.Header header) {
        throw new RuntimeException("Stub!");
    }

    public void startPreferenceFragment(Fragment fragment, boolean push) {
        throw new RuntimeException("Stub!");
    }

    public void startPreferencePanel(String fragmentClass, Bundle args, int titleRes, CharSequence titleText, Fragment resultTo, int resultRequestCode) {
        throw new RuntimeException("Stub!");
    }

    public void finishPreferencePanel(Fragment caller, int resultCode, Intent resultData) {
        throw new RuntimeException("Stub!");
    }

    public boolean onPreferenceStartFragment(PreferenceFragment caller, Preference pref) {
        throw new RuntimeException("Stub!");
    }

    /** @deprecated */
    @Deprecated
    public PreferenceManager getPreferenceManager() {
        throw new RuntimeException("Stub!");
    }

    /** @deprecated */
    @Deprecated
    public void setPreferenceScreen(PreferenceScreen preferenceScreen) {
        throw new RuntimeException("Stub!");
    }

    /** @deprecated */
    @Deprecated
    public PreferenceScreen getPreferenceScreen() {
        throw new RuntimeException("Stub!");
    }

    /** @deprecated */
    @Deprecated
    public void addPreferencesFromIntent(Intent intent) {
        throw new RuntimeException("Stub!");
    }

    /** @deprecated */
    @Deprecated
    public void addPreferencesFromResource(int preferencesResId) {
        throw new RuntimeException("Stub!");
    }

    /** @deprecated */
    @Deprecated
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        throw new RuntimeException("Stub!");
    }

    /** @deprecated */
    @Deprecated
    public Preference findPreference(CharSequence key) {
        throw new RuntimeException("Stub!");
    }

    protected void onNewIntent(Intent intent) {
        throw new RuntimeException("Stub!");
    }

    public static final class Header implements Parcelable {
        public static final Creator<PreferenceActivity.Header> CREATOR = null;
        public CharSequence breadCrumbShortTitle;
        public int breadCrumbShortTitleRes;
        public CharSequence breadCrumbTitle;
        public int breadCrumbTitleRes;
        public Bundle extras;
        public String fragment;
        public Bundle fragmentArguments;
        public int iconRes;
        public long id;
        public Intent intent;
        public CharSequence summary;
        public int summaryRes;
        public CharSequence title;
        public int titleRes;

        public Header() {
            throw new RuntimeException("Stub!");
        }

        public CharSequence getTitle(Resources res) {
            throw new RuntimeException("Stub!");
        }

        public CharSequence getSummary(Resources res) {
            throw new RuntimeException("Stub!");
        }

        public CharSequence getBreadCrumbTitle(Resources res) {
            throw new RuntimeException("Stub!");
        }

        public CharSequence getBreadCrumbShortTitle(Resources res) {
            throw new RuntimeException("Stub!");
        }

        public int describeContents() {
            throw new RuntimeException("Stub!");
        }

        public void writeToParcel(Parcel dest, int flags) {
            throw new RuntimeException("Stub!");
        }

        public void readFromParcel(Parcel in) {
            throw new RuntimeException("Stub!");
        }
    }
}