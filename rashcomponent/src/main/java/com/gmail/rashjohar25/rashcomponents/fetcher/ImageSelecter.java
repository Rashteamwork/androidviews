package com.gmail.rashjohar25.rashcomponents.fetcher;

import android.content.Context;
import android.content.Intent;
import android.provider.MediaStore;


public class ImageSelecter {
    private Context context;
    private int CODE=0;
    public ImageSelecter(Context context) {
        this.context=context;
    }
    public Intent getSelecter(com.gmail.rashjohar25.rashcomponents.fetcher.datatypes.ImageSelecter from)
    {
        if(from.equals(com.gmail.rashjohar25.rashcomponents.fetcher.datatypes.ImageSelecter.Camera))
        {
            CODE=1;
            return  new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        if(from.equals(com.gmail.rashjohar25.rashcomponents.fetcher.datatypes.ImageSelecter.Gallery))
        {
            CODE=2;
            return new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        }
        return null;
    }

    public int getSourceCODE() {
        return CODE;
    }
}
