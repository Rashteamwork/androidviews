package com.gmail.rashjohar25.rashcomponents.widgets.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.util.AttributeSet;

import com.gmail.rashjohar25.rashcomponents.R;
import com.gmail.rashjohar25.rashcomponents.extended.AbstractRecyclerView;

public class GridRecyclerView extends AbstractRecyclerView {
    private int spanCount= GridLayoutManager.DEFAULT_SPAN_COUNT;
    public GridRecyclerView(Context context) {
        super(context,true);
        sameGrid(context,null);

    }

    public GridRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs,true);
        sameGrid(context,attrs);

    }

    public GridRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle,true);
        sameGrid(context,attrs);
    }

    public int getSpanCount() {
        return spanCount;
    }

    public void setSpanCount(int spanCount) {
        this.spanCount = spanCount;
        invalidate();
        requestLayout();
    }

    private void sameGrid(Context context, @Nullable AttributeSet attrs)
    {
        if(attrs!=null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs,
                    R.styleable.GridRecyclerView,
                    0, 0);
            try {
                spanCount = a.getInteger(R.styleable.GridRecyclerView_spancount, GridLayoutManager.DEFAULT_SPAN_COUNT);
            } finally {
                a.recycle();
            }
        }
        setLayoutManager(new GridLayoutManager(context,spanCount,getOrientedby(),getReverseLayout()));
    }
}