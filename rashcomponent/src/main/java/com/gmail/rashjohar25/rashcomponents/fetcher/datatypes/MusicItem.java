package com.gmail.rashjohar25.rashcomponents.fetcher.datatypes;

import java.io.Serializable;

public class MusicItem implements Serializable {
    private String id;
    private String data;
    private String title;
    private String artist;
    private String size;
    private String displayName;
    private String duration;

    public MusicItem(String id, String data, String title, String artist, String size, String displayName, String duration) {
        this.id = id;
        this.data = data;
        this.title = title;
        this.artist = artist;
        this.size = size;
        this.displayName = displayName;
        this.duration = duration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
