package com.gmail.rashjohar0007.rashlibraries;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class LinearRecyclerAdapter extends RecyclerView.Adapter<LinearRecyclerAdapter.LinearVH>{
    private Context context;
    private List<String> messages;

    public LinearRecyclerAdapter(Context context, List<String> messages) {
        this.context = context;
        this.messages = messages;
    }

    @Override
    public LinearVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context).inflate(R.layout.linear_vh,parent,false);
        return new LinearVH(v);
    }

    @Override
    public void onBindViewHolder(LinearVH holder, int position) {
        holder.textView2.setText(messages.get(position));
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class LinearVH extends RecyclerView.ViewHolder{
        public TextView textView2;
        public LinearVH(View itemView) {
            super(itemView);
            textView2=(TextView)itemView.findViewById(R.id.textView2);
        }
    }

}